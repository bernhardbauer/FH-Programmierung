/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_conf.h"

int main(void) {
	// Init Strukturen
	GPIO_InitTypeDef GPIO_InitStruct;
	
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);
	
	// GPIO konfigurieren
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_5;
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_Init(GPIOA, &GPIO_InitStruct);
	
	// User Input definieren
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_13;
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOC, &GPIO_InitStruct);
	
	// User LED abschalten
	GPIO_ResetBits(GPIOA, GPIO_Pin_5);
	
	// Watchdog konfigurieren
	IWDG_WriteAccessCmd(IWDG_WriteAccess_Enable);
	IWDG_SetPrescaler(IWDG_Prescaler_8);
	IWDG_SetReload(0x0FFF);
	IWDG_WriteAccessCmd(IWDG_WriteAccess_Disable);
	IWDG_ReloadCounter();
	
	
	// Busy-Wait bis User Button gedrueckt
	while (GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_13));
	
	// User LED einschalten
	GPIO_SetBits(GPIOA, GPIO_Pin_5);
	
	// Watchdog aktivieren
	IWDG_Enable();
	
	
	while (1) {
		// Wenn in der while Schleife der counter zurueckgesetzt wird, so wird der Watchdog nicht ausgeloest
		//IWDG_ReloadCounter();
	}
}
