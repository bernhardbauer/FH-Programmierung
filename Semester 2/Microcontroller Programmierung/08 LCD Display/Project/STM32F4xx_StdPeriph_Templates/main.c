/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_conf.h"

#define LD4 GPIO_Pin_1
#define LD5 GPIO_Pin_15
#define LD6 GPIO_Pin_14
#define LD7 GPIO_Pin_13
#define LEN GPIO_Pin_3
#define LRS GPIO_Pin_4
#define LRW GPIO_Pin_5

void wait_ms(int ms) {
	// warte ein bisschen
	int i;
	for (i=1; i<ms*20000; i++);
}

void puls_en(void) {
	// erzeugt am EN einen Impuls, zuerst steigend, dann fallend
	wait_ms(1);
	GPIO_SetBits(GPIOB, LEN); // Flanke steigt
	wait_ms(1);
	GPIO_ResetBits(GPIOB, LEN); // Flanke faellt
}

void lcd_init() {
	// hier muessen wir allerhand "Tanz" auffuehren
	// damit das Display einwandfrei hochfaehrt
	// Wichtig, ganz wichtig 4-BIT-MODUS!
	// -> http://sprut.de/electronic/lcd/#init
	
	// Notation: RS,RW - D7 D6 D5 D4 D3 D2 D1 D0
	
	// 0. Schritt: 15ms warten
	wait_ms(15);
	
	// 1. Schritt 00-0011....
	GPIO_ResetBits(GPIOB, LRS|LRW|LD7|LD6);
	GPIO_SetBits(GPIOB, LD5|LD4);
	puls_en();
	
	// 2. Schritt: 00-0011....
	wait_ms(5);
	// Die folgenden Zeilen sind auskommentiert, da sich an den gesetzten Bits ohnehin nichts �ndert
	//GPIO_ResetBits(GPIOB, LRS|LRW|LD7|LD6);
	//GPIO_SetBits(GPIOB, LD5|LD4);
	puls_en();
	
	// 3. Schritt: 00-0011....
	wait_ms(1);
	//GPIO_ResetBits(GPIOB, LRS|LRW|LD7|LD6);
	//GPIO_SetBits(GPIOB, LD5|LD4);
	puls_en();
	GPIO_ResetBits(GPIOB, LRS|LRW|LD7|LD6|LD4);
	GPIO_SetBits(GPIOB, LD5);
	puls_en();
	
	// AB JE-HETZT simma auf 4-bit eingestellt
	// alles, was wir an das Display schicken
	// muss auf zwei Haelften aufgeteilt werden
	
	// 5. Schritt 00-001010xx - FUNCTION SET
	// 1. Haelfte 00-0010
	//GPIO_ResetBits(GPIOB, LRS|LRW|LD7|LD6|LD4);
	//GPIO_SetBits(GPIOB, LD5);
	puls_en();
	// 2. Haelfte 00-10xx
	GPIO_ResetBits(GPIOB, LRS|LRW|LD6);
	GPIO_SetBits(GPIOB, LD7);
	puls_en();
	
	// 6. Schritt 00-00001000 - DISPLAY AUS
	// 1. Haelfte 00-0000
	GPIO_ResetBits(GPIOB, LRS|LRW|LD7|LD6|LD5|LD4);
	//GPIO_SetBits(GPIOB, );
	puls_en();
	// 2. Haelfte 00-1000
	//GPIO_ResetBits(GPIOB, LRS|LRW|LD6|LD5|LD4);
	GPIO_SetBits(GPIOB, LD7);
	puls_en();
	
	// 7. Schritt 00-00000001 - DISPLAY LOESCHEN
	// 1. Haelfte 00-0000
	GPIO_ResetBits(GPIOB, LRS|LRW|LD7|LD6|LD5|LD4);
	//GPIO_SetBits(GPIOB, );
	puls_en();
	// 2. Haelfte 00-0001
	//GPIO_ResetBits(GPIOB, LRS|LRW|LD7|LD6|LD5);
	GPIO_SetBits(GPIOB, LD4);
	puls_en();
	
	// 8. Schritt 00-00000110 - CURSOR NACH RECHTS WANDERND, KEIN DISPLAY SHIFT
	// 1. Haelfte 00-0000
	GPIO_ResetBits(GPIOB, LRS|LRW|LD7|LD6|LD5|LD4);
	//GPIO_SetBits(GPIOB, );
	puls_en();
	// 2. Haelfte 00-0110
	//GPIO_ResetBits(GPIOB, LRS|LRW|LD7|LD4);
	GPIO_SetBits(GPIOB, LD6|LD5);
	puls_en();
	
	// 9. Schritt 00-00001100 - DISPLAY EIN
	// 1. Haelfte 00-0000
	GPIO_ResetBits(GPIOB, LRS|LRW|LD7|LD6|LD5|LD4);
	//GPIO_SetBits(GPIOB, );
	puls_en();
	// 2. Haelfte 00-1100
	//GPIO_ResetBits(GPIOB, LRS|LRW|LD5|LD4);
	GPIO_SetBits(GPIOB, LD7|LD6);
	puls_en();
	
	// ab diesem Punkt ist das Display fertig konfiguriert und
	// kann ganz normal verwendet werden.
	// ON, kein Cursor und der blinkt auch nicht
	// weil der Cursor aber cool ist, schalten wir ihn jetzt ein
	
	// 10. Schritt (Fleissaufgabe) 00-00001111 Cursor cool blinken
	// 1. Haelfte 00-0000
	GPIO_ResetBits(GPIOB, LRS|LRW|LD7|LD6|LD5|LD4);
	//GPIO_SetBits(GPIOB, );
	puls_en();
	// 2. Haelfte 00-1111
	GPIO_SetBits(GPIOB, LD7|LD6|LD5|LD4);
	puls_en();
	
}

void lcd_sendchar(char zeichen) {
	// Char schreiben
	GPIO_SetBits(GPIOB, LRS); // OMG - WIR SCHREIBEN DATEN. DATEN!!
	GPIO_WriteBit(GPIOB, LD7, zeichen&0x80);
	GPIO_WriteBit(GPIOB, LD6, zeichen&0x40);
	GPIO_WriteBit(GPIOB, LD5, zeichen&0x20);
	GPIO_WriteBit(GPIOB, LD4, zeichen&0x10);
	puls_en(); // dieses war der erste Sreich...
	
	GPIO_WriteBit(GPIOB, LD7, zeichen&0x08);
	GPIO_WriteBit(GPIOB, LD6, zeichen&0x04);
	GPIO_WriteBit(GPIOB, LD5, zeichen&0x02);
	GPIO_WriteBit(GPIOB, LD4, zeichen&0x01);
	puls_en();
}

int main(void) {
	GPIO_InitTypeDef GPIO_InitStruct;
	
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
	
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStruct.GPIO_Pin = LD4 | LD5 | LD6 | LD7 | LEN | LRS | LRW;
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_100MHz;
	// und schwupp - zur Init-Funktion
	GPIO_Init(GPIOB, &GPIO_InitStruct);
	
	lcd_init();
	
	lcd_sendchar('h');
	lcd_sendchar('e');
	lcd_sendchar('l');
	lcd_sendchar('l');
	lcd_sendchar('o');
	lcd_sendchar(' ');
	lcd_sendchar(':');
	lcd_sendchar(')');
	
	
	while (1) {}
}
