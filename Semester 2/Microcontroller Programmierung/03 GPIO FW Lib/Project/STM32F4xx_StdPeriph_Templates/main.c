/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_conf.h"

int main(void) {

	// Die Struktur brauchma, um die GPIO_Init aufzurufen...
	GPIO_InitTypeDef GPIO_InitStruct;
	
	// Wir wollen nun die LED zum Leuchten bringen,
	// aber unter Verwendung der FW-Library ... Muhahaha!
	
	// 1. Schritt, GPIO aktivieren	
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
	
	// jetzt... schwierig... GPIO... oje...
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_5;
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_100MHz;
	
	GPIO_Init(GPIOA, &GPIO_InitStruct);
	GPIO_SetBits(GPIOA, GPIO_Pin_5);
	
	
	
	while (1) {}
}
