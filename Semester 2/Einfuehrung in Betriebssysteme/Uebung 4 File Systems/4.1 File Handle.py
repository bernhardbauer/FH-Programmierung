import time

def write_to_file(filename):
	file = open(filename, "w")

	for i in range(25):
		file.write("Write Line: %i, %lf\n" % (i, time.time()))
		time.sleep(0.1)

	file.close()

def append_to_file(filename):
	file = open(filename, "a+")

	for i in range(25):
		file.write("Append Line: %i\n" % i)
		time.sleep(0.1)

	file.close()

def read_from_file(filename):
	file = open(filename, "r")

	for line in file:
		print line

	file.close()


write_to_file("demo.txt")
#append_to_file("demo.txt")
read_from_file("demo.txt")
