# uncompyle6 version 3.2.3
# Python bytecode 3.5 (3350)
# Decompiled from: Python 3.5.3 (default, Jan 19 2017, 14:11:04) 
# [GCC 6.3.0 20170118]
# Embedded file name: check_solution.py
# Compiled at: 2018-06-26 23:30:20
# Size of source mod 2**32: 3456 bytes
import socket, getpass, hashlib, stat, os, sys, pwd, subprocess, time

def find_owner(filename):
    return str(pwd.getpwuid(os.stat(filename).st_uid).pw_name)


def find_group(filename):
    return str(pwd.getpwuid(os.stat(filename).st_gid).pw_name)


def find_permission(filename):
    return str(oct(stat.S_IMODE(os.lstat(filename).st_mode)))


hostname = socket.gethostname()
print('Your hostname: ' + hostname)
username = getpass.getuser()
print('Your username: ' + username)
owner = find_owner(sys.argv[0])
print('Here is the owner of the file: ' + owner)
group = find_group(sys.argv[0])
print('Here is the group of the file: ' + group)
permissions = find_permission(sys.argv[0])
print(permissions)
swap = os.popen("free -k | grep Swap | awk '{print $2}'").read().rstrip()[0]
print('Your Swap is: ' + swap + ' GB')
filename = []
filename.append('2-200.csv')
filename.append('202-400.csv')
filename.append('402-600.csv')
filename.append('602-800.csv')
filename.append('802-1000.csv')
filename.append('1002-1200.csv')
all_sorted_lists = []
all_sorted_lists.append(list(range(2, 201, 2)))
all_sorted_lists.append(list(range(202, 401, 2)))
all_sorted_lists.append(list(range(402, 601, 2)))
all_sorted_lists.append(list(range(602, 801, 2)))
all_sorted_lists.append(list(range(802, 1001, 2)))
all_sorted_lists.append(list(range(1002, 1201, 2)))
for specific_filename in filename:
    if os.path.isfile(specific_filename):
        os.remove(specific_filename)
        print(specific_filename + ' removed')

PIPE = 0
proc = subprocess.Popen([sys.argv[1]], shell=True)
pid = proc.pid + 1
folder = '/proc/' + str(pid) + '/task'
time.sleep(0.5)
multithreading = 'multithreading is NOT active'
while 1:
    if proc.poll() is None:
        try:
            check_multithreading = len(next(os.walk(folder))[1])
            if check_multithreading > 2:
                multithreading = 'multithreading is active'
                print(multithreading)
                proc.wait()
        except:
            print(multithreading)

sort_counter = 0
for specific_filename, sorted_list in zip(filename, all_sorted_lists):
    if os.path.isfile(specific_filename) and find_permission(specific_filename) == '0o600' and find_owner(specific_filename) == username and find_group(specific_filename) == username:
        print('\n' + specific_filename + ' right permissions!')
        fd_read = open(specific_filename)
        fileArray = []
        for line in fd_read:
            fileArray.append(int(line))

        if fileArray == sorted_list:
            print(specific_filename + ' right sorted and calculated!')
            sort_counter = sort_counter + 1
        else:
            print(specific_filename + ' WRONG sorted or calculated!')
    else:
        print(specific_filename + ' WRONG permissions!')

solution_output = 'hostname: ' + hostname + '\nusername: ' + username + '\nowner: ' + owner + '\ngroup: ' + group + '\npermissions: ' + permissions + '\nSwap: ' + swap + '\n' + multithreading + '\nsorted: ' + str(sort_counter) + '\n'
solution = '@^@^@test@^@^@' + solution_output + '@^@^@test@^@^@'
hashcheck = hashlib.sha256(solution.encode('utf-8')).hexdigest()
print('------------------------------------------------------------------------------------')
print(solution_output + '\n' + hashcheck)
print('------------------------------------------------------------------------------------')
# okay decompiling check_solution.pyc
