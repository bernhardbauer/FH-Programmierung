#!/usr/bin/env python3
"""This is the solution to Fernlehre 3"""
import threading
import subprocess
import glob
import os

# Semaphore for the calc.sh file
SEMAPHORE = threading.Semaphore(value=2)

def execute(cmd):
    """Execute shell command"""
    process = subprocess.Popen([cmd], shell=True, stdout=subprocess.PIPE)
    return process.communicate()

def read_from_file(r_filename):
    """Read contents from file and return it as array"""
    file = open(r_filename, "r")
    content = []

    for line in file:
        content.append(int(line.strip()))

    file.close()
    return content

def write_to_file(w_filename, content):
    """Write contents to file"""
    file = open(w_filename, "w")
    file.write(content)
    file.close()

def calc(num):
    """Multiply the number using calc script"""
    (out, err) = execute('./calc.sh ' + str(num))
    if err != None:
        print(err)
    return int(out)

def slowsort(arr, i, j):
    """Implementation of the slowsort algo"""
    # print(arr, i, j)
    if i >= j:
        return

    m_pylint = (int)((i + j) / 2)

    slowsort(arr, i, m_pylint)
    slowsort(arr, m_pylint + 1, j)

    if arr[j] < arr[m_pylint]:
        t_pylint = arr[j]
        arr[j] = arr[m_pylint]
        arr[m_pylint] = t_pylint

    slowsort(arr, i, j - 1)

def slowsort_start(s_filename):
    """Start the slow sort algorithm"""
    SEMAPHORE.acquire()
    # Read file as int[] and apply calc.sh
    arr = []
    for i in read_from_file(s_filename):
        arr.append(calc(i))

    # Sort array
    slowsort(arr, 0, len(arr) - 1)

    # Generate the new filename
    from_to_csv_name = os.path.basename(s_filename).replace(".csv", "").split("-")
    new_filename = "%d-%d.csv" % (int(from_to_csv_name[0]) * 2, int(from_to_csv_name[1]) * 2)

    file_content = ''
    for i in arr:
        file_content += str(i) + '\n'
    write_to_file(new_filename, file_content)

    execute('chmod 600 ' + new_filename)
    SEMAPHORE.release()



# Start the program
if __name__ == '__main__':
    for filename in sorted(glob.glob("*.csv")):
        os.remove(filename)

    execute('chmod +x ./gencsv.sh ./calc.sh')
    execute('./gencsv.sh')

    THREADS = []
    for filename in sorted(glob.glob("*.csv")):
        thread = threading.Thread(target=slowsort_start, args=(filename,))
        thread.start()
        THREADS.append(thread)

    for thread in THREADS:
        thread.join()
