#!/usr/bin/python2.7

from threading import Thread, Lock
import time
import sys
import random

mutex = Lock()

def calc_random(threadName, values):
	a = [None] * values
	b = [None] * values
	c = [None] * values

	for i in range(values):
		a[i] = random.randint(0, 9)
		b[i] = random.randint(0, 9)
		c[i] = a[i] + b[i]

		time.sleep(0.5)
		print >> sys.stderr, threadName + ": %i + %i = %i" % (a[i], b[i], c[i])

	mutex.acquire()
	print "Hello my name is: %s" % (threadName)
	for i in range(values):
		print threadName + ": c[i] = %i" % (c[i])
	mutex.release()

# Create threads
threads = []
thread_count = 3

if len(sys.argv) > 1:
	try:
		thread_count = int(sys.argv[1])
		print "Thread count set to %i" % (thread_count)
	except Exception as e:
		thread_count = 3
		print "Invalid thread count given. Using 3 Threads."

for i in range(thread_count):
	t = Thread(target=calc_random, args=("Thread-"+str(i), 5))
	threads.append(t)
	t.start()

for i in range(thread_count):
	threads[i].join()
