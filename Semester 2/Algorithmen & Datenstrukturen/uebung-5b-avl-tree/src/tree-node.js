
export default class TreeNode {

	constructor(key, data) {
		this.key = key; // Int key
		this.data = data; // Daten (String)
		this.left = null; // Node
		this.right = null; // Node
	}

	assignLeftNode = (node) => {
		if (node instanceof TreeNode) {
			this.left = node;
			return true;
		}
		return false;
	}

	assignRightNode = (node) => {
		if (node instanceof TreeNode) {
			this.right = node;
			return true;
		}
		return false;
	}

	removeLeftNode = (node) => {
		if (node instanceof TreeNode) {
			this.left = null;
			return true;
		}
		return false;
	}

	removeRightNode = (node) => {
		if (node instanceof TreeNode) {
			this.right = null;
			return true;
		}
		return false;
	}

}
