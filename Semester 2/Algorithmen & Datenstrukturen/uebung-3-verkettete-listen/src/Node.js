export default class Node {
	constructor(next, previous, key, data) {
		this.next = next;
		this.previous = previous;
		this.key = key;
		this.data = data;
	}
}
