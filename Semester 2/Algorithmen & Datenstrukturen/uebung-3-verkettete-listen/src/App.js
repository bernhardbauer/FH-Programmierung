import React, { Component } from 'react';
import faker from 'faker';
import './App.css';
import LinkedList from './LinkedList.js';

class App extends Component {

	state = {
		list: null,
		lastSearch: null
	};

	componentDidMount() {
		let list = new LinkedList();
		for (var i = 0; i < 100; i++) {
			list.insertEnd(i, faker.fake("{{name.firstName}} {{name.lastName}}"));
		}
		this.setState({ list: list });
	}

	createListObjects(node) {
		// generate content;
		let contentArray = [];
		let content = (
			<div
				key={ node.key }
				data-key={ node.key }
				onClick={ this.onRemove }
				className={ (this.state.lastSearch !== null && this.state.lastSearch === node.key) ? "listItem highlighted" : "listItem" } >
				<b>{ node.key }</b>: { node.data }
			</div>
		);
		contentArray.push(content);

		// generate for next item
		if (node.next !== null) {
			contentArray.push(...this.createListObjects(node.next));
		}

		// return generated;
		return contentArray;
	}

	onInsert = (event) => {
		if (this.state.list !== null) {
			const keyValue = parseInt(document.getElementById('keyInput').value, 0);
			const dataValue = document.getElementById('valueInput').value;

			if (!isNaN(keyValue) && dataValue) {
				let llist = this.state.list;
				if (llist.insertBefore(keyValue, dataValue)) {
					this.setState({ list: llist });
					console.log("[List Insert] Some data with key "+keyValue+" has been inserted.");

					// Clear inputs
					document.getElementById('keyInput').value = "";
					document.getElementById('valueInput').value = "";
				} else {
					console.log("[List Insert] Could not insert data.");
				}
			} else {
				console.log("[List Insert] Insufficient data provided!");
			}
		}
	}

	onSearch = (event) => {
		if (this.state.list !== null) {
			const keyValue = parseInt(document.getElementById('keyInput').value, 0);
			if (keyValue) {
				this.setState({ lastSearch: keyValue });

				const searchResult = this.state.list.findNode(keyValue);
				console.log("[List Search] "+keyValue+": ", searchResult);
				if (searchResult === null) {
					alert("Der eingegebene Wert wurde nicht gefunden.");
				}
			} else {
				console.log("[List Search] No key given");
			}
		}
	}

	onRemove = (event) => {
		if (this.state.list !== null) {
			this.state.list.delete(parseInt(event.target.getAttribute('data-key'), 0));
			this.setState({ list: this.state.list });
		}
	}

	render() {
		let listCount = 0;
		if (this.state.list !== null) {
			listCount = this.state.list.count;
		}

		let content;
		if (this.state.list !== null && this.state.list.head !== null) {
			content = this.createListObjects(this.state.list.head);
		}

		return (
			<div className="App">
				<div className="insertPane">
					<h1>Linked List (Contains { listCount } Element(s))</h1>
					<input type="text" id="keyInput" placeholder="Key" />
					<input type="text" id="valueInput" placeholder="Value" />
					<input type="submit" value="Einfügen" onClick={this.onInsert} />
					<input type="submit" value="Nach Key Suchen" onClick={this.onSearch} />
				</div>
				{ content }
			</div>
		);
	}
}

export default App;
