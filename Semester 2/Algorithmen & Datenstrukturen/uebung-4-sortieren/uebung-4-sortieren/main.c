#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define true 1
#define false 0
typedef int bool;

struct _element {
	char icao_code[5];
	char station_name[100];
};

typedef struct _element element;


void insertion_sort(element* stations, int size) {
	// implement either insertion_sort or selection_sort
    for (int i=2; i<size; i++) {
        element value = stations[i];
        int j = i;
        
        while (j>=1 && strcmp(stations[j-1].station_name, value.station_name) > 0) {
            stations[j] = stations[j - 1];
            --j;
            stations[j] = value;
        }
    }
}

void selection_sort(element* stations, int size) {
	// implement either insertion_sort or selection_sort
}

int partition(element *stations, int left, int right)
{
    int i = left - 1;
    element piv = stations[right];
    
    for (int j = left; j < right; j++) {
        if (strcmp(piv.station_name, stations[j].station_name) > 0) {
            i++;
            element tmp = stations[i];
            stations[i] = stations[j];
            stations[j] = tmp;
        }
    }
    
    element tmp = stations[i+1];
    stations[i+1] = stations[right];
    stations[right] = tmp;
    
    return i+1;
}

void quick_sort(element *stations, int left, int right) {
    if (left < right) {
        int p = partition(stations, left, right);
        quick_sort(stations, left, p-1);
        quick_sort(stations, p+1, right);
    }
}


void merge_sort(element *stations, element *tmpStations, int left, int right)
{
    // implement either quick_sort or merge_sort
    // tmpStations corresponds to the array B in the lecture slides
}



void print_stations(element* stations, int size) {
	int i;
	for (i = 0; i < size; i++) {
		printf("%s : %s", stations[i].icao_code, stations[i].station_name);
	}
}

int readfile(element* stations, int* size) {
	FILE * fp;
	char line[100]; // keeps one line of the file

	// try to open the file; in case of an error exit the program
	*size = 0;
	fp = fopen("stations.csv", "r");
	if (!fp) {
		printf("Cannot open file stations.csv!\n");
		return 1;
	}
	
	// read all weather stations from file and add them to the 
	// hash table
	while (!feof(fp)) {
		// read one line and skip empty and comment lines
		fgets(line, 100, fp);
		if (line[0] == '#') continue;
		if (isspace(line[0])) continue;
		
		// take the first 4 characters as key value
		strncpy(stations[*size].icao_code, line, 4);
		stations[*size].icao_code[4] = '\0';
		
		// take the rest as value
		strcpy(stations[*size].station_name, strchr(line, ';')+1);
		*size += 1;
	}
	
	// close the file 
	fclose(fp);
	
	return 0;
}

int main(int argc, char** argv) {
	element* stations; // holds the station codes and names
	element* tmpStations; // holds temporary station codes and names for merge sort
	int size; // holds the number of valid entries in the array
		
	stations = (element*)malloc(sizeof(element)*6000);
	tmpStations = (element*)malloc(sizeof(element)*6000);
	
    // now lets try insertion sort
    printf("Sorting with insertion sort:\n\n");
    // read the station data from file
    readfile(stations, &size);
    // sort the station names
    insertion_sort(stations, size);
    // print the result
    print_stations(stations, size);
//
//    // now lets try selection sort
//    printf("Sorting with selection sort:\n\n");
//    // read the station data from file
//    readfile(stations, &size);
//    // sort the station names
//    selection_sort(stations, size);
//    // print the result
//    print_stations(stations, size);
//
//    // now lets try merge sort
//    printf("Sorting with merge sort:\n\n");
//    // read the station data from file
//    readfile(stations, &size);
//    // sort the station names
//    merge_sort(stations, tmpStations, 0, size-1);
//    // print the result
//    print_stations(stations, size);
//
    // now lets try quick sort
    printf("Sorting with quick sort:\n\n");
    // read the station data from file
    readfile(stations, &size);
    // sort the station names
    quick_sort(stations, 0, size-1);
    // print the result
    print_stations(stations, size);

    free(stations);
    free(tmpStations);
	
	return 0;
}
