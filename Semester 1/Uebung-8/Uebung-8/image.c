/*
 *  image.c
 *
 */

#include "image.h"

struct pixel* readBitmap(const char *filename, struct header* pHeader, struct info* pInfo)
{
	struct pixel *pData;
	FILE* fp;
	
	fp = fopen(filename, "rb");
	if (fp == NULL) {
		printf("Datei %s kann nicht geoeffnet werden!\n", filename);
		return NULL;
	}
	
	/* fread dient zum Einlesen von Binärdateien. fread-Parameter:
	 1.: Zeiger auf den Speicherbereich in den die eingelesenen Daten geschrieben werden
	 2.: Groesse eines Elements welches eingelesen werden soll
	 3.: Anzahl der Elemente die eingelesen werden sollen
	 4.: Datei von der gelesen werden soll
	 */
	/* elemente muessen einzeln eingelesen werden, da ints immer an einer
	 4-byte grenze ausgerichtet werden und das erste feld nur aus 2 byte besteht */
	fread(&pHeader->bfType, sizeof(char), 2, fp);
	fread(&pHeader->bfSize, sizeof(unsigned int), 1, fp);
	fread(&pHeader->bfReserved, sizeof(unsigned int), 1, fp);
	fread(&pHeader->bfOffBits, sizeof(unsigned int), 1, fp);
	fread(pInfo, sizeof(struct info), 1, fp);
    
    pData = (struct pixel*) malloc(pInfo->biSizeImage);
    if (pData == NULL) {
        printf("Error allocating memory for image data\n");
        return NULL;
    }
    fread(pData, sizeof(struct pixel), pInfo->biHeight * pInfo->biWidth, fp);
    
	fclose(fp);
	
	return pData;
}

void writeBitmap(const char *filename, struct header* pHeader, struct info* pInfo, struct pixel* pData)
{
    if (pData == NULL) {
        printf("Could not write output file! No file content given.\n");
        return;
    }
    
	FILE* fp;
	
	fp = fopen(filename, "wb");
	if (fp == NULL) {
		printf("Datei %s kann nicht geoeffnet werden!\n", filename);
		return;
	}
    
    fwrite(&pHeader->bfType, sizeof(char), 2, fp);
    fwrite(&pHeader->bfSize, sizeof(unsigned int), 1, fp);
    fwrite(&pHeader->bfReserved, sizeof(unsigned int), 1, fp);
    fwrite(&pHeader->bfOffBits, sizeof(unsigned int), 1, fp);
    fwrite(pInfo, sizeof(struct info), 1, fp);
    
    printf("Size: %d\n", pInfo->biSizeImage);
    fwrite(pData, sizeof(struct pixel), pInfo->biHeight * pInfo->biWidth, fp);
	
	fclose(fp);
}

struct pixel* mirror(struct info* pInfo, struct pixel* pData)
{
    struct pixel* pDataInverted = (struct pixel*) malloc(pInfo->biSizeImage);
    
    for (int i=0; i<(pInfo->biSizeImage/sizeof(struct pixel)); i++) {
        *(pDataInverted + i) = *(pData + (pInfo->biSizeImage/3) - i);
    }
    return pDataInverted;
}

struct pixel* invert(struct info* pInfo, struct pixel* pData)
{
    struct pixel* pDataInverted = (struct pixel*) malloc(pInfo->biSizeImage);
    
    for (int i=0; i<(pInfo->biSizeImage/sizeof(struct pixel)); i++) {
        (pDataInverted + i)->red = ~(pData + i)->red;
        (pDataInverted + i)->green = ~(pData + i)->green;
        (pDataInverted + i)->blue = ~(pData + i)->blue;
    }
	return pDataInverted;
}

struct pixel* saturation(struct info* pInfo, struct pixel* pData, double wRed, double wGreen, double wBlue)
{
    struct pixel* pDataInverted = (struct pixel*) malloc(pInfo->biSizeImage);
    
    for (int i=0; i<(pInfo->biSizeImage/sizeof(struct pixel)); i++) {
        (pDataInverted + i)->red = (pData + i)->red * wRed;
        (pDataInverted + i)->green = (pData + i)->green * wGreen;
        (pDataInverted + i)->blue = (pData + i)->blue * wBlue;
    }
    return pDataInverted;
}


