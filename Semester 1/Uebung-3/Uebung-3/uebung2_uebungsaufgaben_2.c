#include <stdio.h>
#include <time.h>
#include <stdlib.h>


int main(void)
{

	int randomnum = 0;  // Zufallzahl welche wir muessen erraten
	int guessed_num = 0; // die Zahl wir ueber die Taste eingeben
	int counter = 0;  // Versuchezaehler fuer der Schleife

	srand(time(NULL));  //this function randomize the seed
	rand();  // ohne rand() wird auch funktionieren
	randomnum = 1 + (int)(100.0*rand() / (RAND_MAX + 1.0));  //rand() % 100 + 1;  --> konnte auch funktionieren

	printf("Welche Zahl ist in meinem Kopf, Sie haben 10 Versuche:");

	for (counter = 0; counter < 10 ; counter++)  //Ich habe nur 10 Versuche erlaubt
	{

		scanf("%i", &guessed_num);

		if (guessed_num == randomnum)
		{
			printf("Tada!!!!!! Nach %i Versuche Sie haben es erraten  ;) \n Deine Zufallszahl: -- %i --\n\n:)\n\n", counter, randomnum);
			break;
		}

		if (guessed_num < randomnum)
			printf("Ihre Wahlzahl ist zu klein, versuchen Sie es noch ein mal:");

		if (guessed_num > randomnum)
			printf("Ihre Wahlzahl ist zu gross, versuchen Sie es noch ein mal:");

	}

	return 0;
}
