//
//  main.cpp
//  Uebung-3
//
//  Created by Bernhard Bauer on 30.11.17.
//  Copyright © 2017 bbmk IT solutions gmbh. All rights reserved.
//

#include <iostream>

using namespace std;

int reverseInt(int reversable) {
    int val = 0;
    
    while (reversable > 0) {
        val = val*10 + reversable % 10;
        reversable /= 10;
    }
    
    return val;
}

int main(int argc, const char * argv[]) {
    int reversable = 0;
    cout << "Geben Sie die umzukehrende Zahl ein: " << endl;
    cin >> reversable;
    cout << reverseInt(reversable) << endl;
    return 0;
}
