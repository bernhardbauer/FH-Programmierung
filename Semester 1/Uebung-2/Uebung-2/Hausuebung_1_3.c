#include <stdio.h>

int main()
{
	int x, y;
	int i, j;

	printf("Geben Sie bitte die erste Zahl des Intervalles und druecken Sie die Intro-Taste:\n");
	scanf("%i", &x);
	printf("Geben Sie bitte die zweite Zahl des Intervalles und druecken Sie die Intro-Taste:\n");
	scanf("%i", &y);
	printf("Die Primzahle zwischen %i und %i sind:\n", x, y);

	for (i = x; i <= y; i++ )
	{
		for (j = 2; j < i; j++)
		{
			if (i%j == 0)
			{
				break;
			}
			
		}
		if (j == i)
		{
			printf("-- %i --", i);
		}

	}
	printf("\n");

	return 0;
}
