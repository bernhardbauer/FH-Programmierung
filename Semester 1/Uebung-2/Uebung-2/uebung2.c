#include <stdio.h>

int main()
{
	int mess = 0;
	int meter = 0;
	int vol = 1;

	printf("Geben Sie bitte die Laenge, Breite und Tiefe Ihres Schwimmbeckens an:\n");

	// mess ist die Messung, meter die 3 Reihen der Matrix, mit 1 Spalte

	for (meter = 0; meter < 3; meter++)
	{
		printf("Messung in Meter:");
		scanf("%i", &mess);
		vol = vol*mess;
	}

	printf("Das Volumen des Schwimmbeckens in Liters ist: %i \n", vol*1000);
	return 0;
}