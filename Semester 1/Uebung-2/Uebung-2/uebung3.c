#include <stdio.h>

int main()

{
	int num1;
	int num2;
	int smaller;

	printf("Gibt die erste Zahl ein und drucken Sie Intro:\n");
	scanf("%i", &num1);
	printf("Gibt die zweite Zahl ein und drucken Sie Intro:\n");
	scanf("%i", &num2);

	if (num1 > num2)
	{
		smaller = num2;
		printf("Die kleinste Zahl ist: %i", smaller);
	}

	else
	{
		smaller = num1;
		printf("Die kleinste Zahl ist: %i \n", smaller);
	}

	return 0;
}