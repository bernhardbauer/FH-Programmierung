#include <stdio.h>

int main()
{
	int jahr = 0;

	printf("Gibt ein Jahr ein und drucken Sie die Taste Intro: ");
	scanf("%i", &jahr);

	if (jahr % 4 == 0) {
        if (jahr % 100 == 0) {
            if (jahr % 400 == 0) {
                printf("%i ist ein Schaltjahr\n", jahr);
            } else {
                printf("%i ist kein Schaltjahr\n", jahr);
            }
        } else {
            printf("%i ist ein Schaltjahr\n", jahr);
        }
    } else {
        printf("%i ist kein Schaltjahr\n", jahr);
    }

	

	return 0;
}
