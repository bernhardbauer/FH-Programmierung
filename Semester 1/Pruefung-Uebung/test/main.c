//
//  main.c
//  test
//
//  Created by Bernhard Bauer on 15.01.18.
//  Copyright © 2018 bbmk IT solutions gmbh. All rights reserved.
//

#include <stdio.h>
#include <stdbool.h>
#include <math.h>

#define square(x) (x)*(x)

struct address {
    char *city, *country, *street;
};

struct person {
    char *firstname, *lastname;  struct address *home, *work;  struct person *mother, *father;
};

bool sum_diff(int first, int second, float *sum, float *diff) {
    if (sum != NULL && diff != NULL) {
        *sum = first + second;
        *diff = first - second;
        
        return true;
    }
    
    return false;
}

void bubbleSort(int list[], int size) {
    int temp, i, j;
    // loop through all numbers
    for(i = 0; i < size-1; i++) {
        // loop through numbers falling ahead
        for(j = 0; j < size-1-i; j++) {
            if(list[j] > list[j+1]) {
                int tmp = list[j];
                list[j] = list[j+1];
                list[j+1] = tmp;
            }
        }
    }
}

void * xxxxxx(void * dst, void const * src, size_t len) {
    long *plDst = (long *)dst;
    long const *plSrc = (long const *)src;
    if (((long)src % sizeof(long)) == 0 && ((long)dst % sizeof(long)) == 0) {
        while (len >= 4) {
            *plDst++ = *plSrc++;
            len -= 4;
        }
    }
    char *pcDst = (char *)plDst;
    char const *pcSrc = (char const *)plSrc;
    while (len--) {
        *pcDst++ = *pcSrc++;
    }
    return dst;
}

int main() {
    
    float src[] = {5.0, 2.5, 8.3, 9.6};
    float dst[100];
    
    void *xxx = xxxxxx(dst, src, 4);
    
    int list[] = { 6, 2, 8, 3, 7, 4, 9, 0, 1, 5 };
    bubbleSort(list, 10);
    
    printf("Bubble sort:");
    for (int i=0; i<sizeof(list)/sizeof(int); i++) {
        printf(" %i", list[i]);
    }
    printf("\n");
    
    int x[] = {5, 2, 5, 1, 5, 0, 5, 2, 5, 1, 5, 0};
    int *p = x + 6;
    while(*p) {
        printf("%i ", *p);
        p++;
    }
    printf("\n");
    
    int result;
    result = 64 / square(8);
    printf("%i\n", result);
    
    struct address addr;
    addr.city = "Wien";
    addr.country = "AT";
    addr.street = "Strasse";
    
    struct person p_mother;
    p_mother.work = &addr;
    
    printf("%s\n", (*p_mother.work).country);
    
    
    printf("POW:%f\n", pow(2, 4));
    
    int first = 10, second = 5;
    float sum = 0, diff = 0;
    sum_diff(first, second, &sum, &diff);
    printf("F:%i; S:%i; SUM:%f; DIFF:%f\n", first, second, sum, diff);
    for (int i=0; i<6; i++) {
        for (int j=3; j<(6+i); j++) {
            if(j==3) {
                printf("%i", j%3);
            } else {
                printf(" %i", j%3);
            }
        }
        printf("\n");
    }
    
    return 0;
}
