//
//  my_math.c
//  Uebung-4
//
//  Created by Bernhard Bauer on 12.12.17.
//  Copyright © 2017 Bernhard Bauer. All rights reserved.
//

#include "my_math.h"

double f(double x) {
    return pow(x, 3) - 4;
}

double df(double x) {
    return 0.0;
}

double bisection(double x0, double x1, double epsilon) {
    // Check if calculation is possible, if not possible return 0.0
    if (f(x0) * f(x1) >= 0) {
        return 0.0;
    }
    
    int count = 0;
    double closest_result = 0.0;
    
    // Search
    while ((x1 - x0) >= epsilon && count < 1000) {
        count++;
        
        double x_neu = (x0 + x1) / 2.0;
        closest_result = f(x_neu);
        double f_x0 = f(x0);
        double f_x1 = f(x1);
        
        // Check if the function is finished or set a new x0 or x1
        if (closest_result * f_x0 >= 0 && closest_result * f_x1 >= 0) {
            break;
        } else if (closest_result * f_x0 < 0) {
            x1 = x_neu;
        } else {
            x0 = x_neu;
        }
    }
    
    printf("Search finished after %i iterations.\n", count);
    printf("x0: %f\n", x0);
    printf("x1: %f\n", x1);
    printf("N: %f\n", (x0+x1)/2);
    
    return closest_result;
}

