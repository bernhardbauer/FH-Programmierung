//
//  my_math.h
//  Uebung-4
//
//  Created by Bernhard Bauer on 12.12.17.
//  Copyright © 2017 Bernhard Bauer. All rights reserved.
//

#ifndef my_math_h
#define my_math_h

#include <stdio.h>
#include <math.h>

double f(double x);
double df(double x);
double bisection(double x0, double x1, double epsilon);

#endif /* my_math_h */
