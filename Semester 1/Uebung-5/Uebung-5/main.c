//
//  main.c
//  Uebung-5
//
//  Created by Bernhard Bauer on 18.12.17.
//  Copyright © 2017 Bernhard Bauer. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <ctype.h>

void swap(float *a, float *b) {
    float tmp = *a;
    *a = *b;
    *b = tmp;
    
}

void memswap(char *mem1, char *mem2, int size) {
    void* tmp = malloc(size);
    
    memcpy(tmp, (void*) mem1, size);
    memcpy((void*) mem1, (void*) mem2, size);
    memcpy((void*) mem2, tmp, size);
}

int firstPosOfChar(char *text, char c) {
    for (int i = 0; i < strlen(text); i++) {
        if (*(text + i) == c) {
            return i;
        }
    }
    return 0;
}

void capitalize(char *text) {
    *text = toupper((unsigned char) *(text));
    
    for (int i = 1; i < strlen(text); i++) {
        if (*((text + i) -1 ) == 0x20) {
            *(text + i) = toupper((unsigned char) *(text + i));
        }
    }
}



int main(int argc, const char * argv[]) {
    
    // a und b vertauschen
    float a = 0.19842;
    float b = 234.23423;
    printf("Austausch von a (%f) und b (%f)\n", a, b);
    swap(&a, &b);
    printf("Nach dem Tausch: a (%f) und b (%f)\n\n", a, b);
    
    // mem1 und mem2 vertauschen
    char mem1[] = "Hello\0";
    char mem2[] = "World\0";
    printf("Austausch von mem1 (%s) und mem2 (%s)\n", mem1, mem2);
    memswap(mem1, mem2, 5);
    printf("Nach dem Tausch: mem1 (%s) und mem2 (%s)\n\n", mem1, mem2);
    
    // Erste position von w in hello world
    char hello[] = "Hello World!\0";
    printf("Erste Position von 'W' in %s an Stelle %i.\n\n", hello, firstPosOfChar(hello, 'W'));
    
    // Capitalize string
    char lorem[] = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam\0";
    printf("Original: %s\n", lorem);
    capitalize(lorem);
    printf("Capitalized: %s\n\n", lorem);
    
    return 0;
}
