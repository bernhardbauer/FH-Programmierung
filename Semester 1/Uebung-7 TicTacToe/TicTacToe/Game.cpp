//
//  Game.cpp
//  TicTacToe
//
//  Created by Bernhard Bauer on 04/11/2017.
//  Copyright © 2017 bbmk IT solutions gmbh. All rights reserved.
//

#include "Game.hpp"
#include "Player.hpp"

Game::Game() {
    cout << "Initializing game TicTacToe\n";
    
    // Initialize game_board
    this->game_board = new Player**[this->game_size];
    for(int i = 0; i < this->game_size; ++i) {
        this->game_board[i] = new Player*[this->game_size];
        for(int j = 0; j < this->game_size; ++j) {
            this->game_board[i][j] = NULL;
        }
    }
    
    // Init rand
    srand(time(NULL));
    rand();
}

Game::~Game() {
    delete this->game_board;
}

bool Game::addPlayer(Player player) {
    bool player_added = false;
    cout << "Adding player '" << player.getName() << "' to game: ";
    
    for (int i=0; i<2; i++) {
        if (this->players[i] == nullptr) {
            this->players[i] = &player;
            player_added = true;
            break;
        }
    }
    
    cout << (player_added ? "true" : "false") << endl;
    return player_added;
}

Player *Game::move(int x, int y, Player *player) {
    if(this->game_board[x][y] == NULL) {
        this->game_board[x][y] = player;
        this->move_count++;
    }
    
    //check col
    for(int i = 0; i < this->game_size; i++) {
        if(this->game_board[x][i] != player)
            break;
        if(i == this->game_size-1) {
            this->winner = player;
            this->is_finished = true;
            return player;
        }
    }
    
    //check row
    for(int i = 0; i < this->game_size; i++) {
        if(this->game_board[i][y] != player)
            break;
        if(i == this->game_size-1) {
            this->winner = player;
            this->is_finished = true;
            return player;
        }
    }
    
    //check diag
    if(x == y){
        //we're on a diagonal
        for(int i = 0; i < this->game_size; i++) {
            if(this->game_board[i][i] != player)
                break;
            if(i == this->game_size-1) {
                this->winner = player;
                this->is_finished = true;
                return player;
            }
        }
    }
    
    //check anti diag
    if((x + y) == (this->game_size - 1)) {
        for(int i = 0;i<this->game_size;i++) {
            if(this->game_board[i][(this->game_size-1)-i] != player)
                break;
            if(i == this->game_size-1) {
                this->winner = player;
                this->is_finished = true;
                return player;
            }
        }
    }
    
    //check draw
    if(this->move_count == pow(this->game_size, 2)) {
        this->is_finished = true;
        return NULL;
    }
    
    return NULL;
}

Player *Game::simulateMove(int x, int y, Player *player) {
    if(this->game_board[x][y] == NULL) {
        Player *p = this->move(x, y, player);
        this->game_board[x][y] = NULL;
        this->move_count--;
        this->winner = NULL;
        this->is_finished = false;
        
        return p;
    }
    
    return NULL;
}

int Game::checkIfPlayerCouldWin(Player *player) {
    for (int i=0; i < pow(this->game_size, 2); i++) {
        Player *p = this->simulateMove(i/this->game_size, i%this->game_size, player);
        if (p != NULL) {
            if (p == player) {
                return i;
            }
        }
    }
    
    return -1;
}

bool Game::isFinished() {
    return this->is_finished;
}

int Game::loop() {
    // Used to switch between the players
    int current_player = 0;
    int player_retries = 0;
    
    while (!this->isFinished()) {
        // Draw the current ui to the console
        this->console_ui.drawFromArray(this->game_board, this->game_size);
        
        // Retreive the move and remap it to 0-8
        cout << "Next move for " << this->players[current_player%2]->getName() << ": ";
        int move = -1;
        
        if (!this->players[current_player%2]->isAI()) {
            move = this->players[current_player%2]->getUserInput() - 1;
        } else {
            // Compute AI input
            
            // Check if is is possible for the current player to win
            move = this->checkIfPlayerCouldWin(this->players[current_player%2]);
            
            // Check if it is possible for the opponent to win
            if (move == -1) {
                move = this->checkIfPlayerCouldWin(this->players[1-(current_player%2)]);
            }
            
            // If the move is -1 randomly choose a place on the board
            if (move == -1) {
                for (int j=0; j < pow(this->game_size, 2); j++) {
                    move = (int)(pow(this->game_size, 2)*rand()/(RAND_MAX+1.0));
                    
                    // If the move is possible, break and continue processing
                    if (this->game_board[move/this->game_size][move%this->game_size] == NULL) {
                        break;
                    }
                }
                // If the move still is -1 just use the first available place on the board
                if (move == -1) {
                    for (int j=0; j < pow(this->game_size, 2); j++) {
                        // If the move is possible, break and continue processing
                        if (this->game_board[j/this->game_size][j%this->game_size] == NULL) {
                            move = j;
                            break;
                        }
                    }
                }
            }
            cout << move+1 << " (AI)" << endl;
        }
        
        // Add the move to the game board
        if (move > -1 && this->game_board[move/this->game_size][move%this->game_size] == NULL) {
            this->move(move/this->game_size, move%this->game_size, this->players[current_player%2]);
        } else if(player_retries < 3) {
            // If there was no move let the player re-enter a number
            cout << "Do not choose fields which have already been chosen by another player! " << 3 - player_retries << " retries left." << endl;
            player_retries++;
            continue;
        }
        
        // Switch to the other player
        current_player++;
        player_retries = 0;
    }
    
    // Draw the current ui to the console
    this->console_ui.drawFromArray(this->game_board, this->game_size);
    // Display game over message
    cout << "Game Over" << endl;
    if (this->winner != NULL) {
        cout << "Player '" << this->winner->getName() << "' has won the game. Congrats ;)" << endl;
    } else {
        cout << "It's a draw." << endl;
    }
    
    return 0;
}
