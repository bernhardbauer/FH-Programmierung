//
//  Player.cpp
//  TicTacToe
//
//  Created by Bernhard Bauer on 04/11/2017.
//  Copyright © 2017 bbmk IT solutions gmbh. All rights reserved.
//

#include "Player.hpp"

Player::Player(string name, bool is_ai) {
    cout << "Initializing player with name " << name << endl;
    this->name = name;
    this->is_ai = is_ai;
}

string Player::getName() {
    return this->name;
}

void Player::setGameCharacter(string character) {
    this->character = character;
}

string Player::getGameCharacter() {
    return this->character;
}

bool Player::isAI() {
    return this->is_ai;
}

int Player::getUserInput() {
    int input = 0;
    
    // Retreive user input (max. 3 retries)
    for (int i=0; i<3; i++) {
        if (!(cin >> input)) {
            cout << "Die Eingabe konnte nicht gelesen werden! Bitte versuchen Sie es erneut: ";
            cin.clear(); // Reset stream
            // Ignore rest of line
            cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        } else {
            break;
        }
    }
    
    return this->sanitizeInput(input);
}

int Player::sanitizeInput(int in) {
    if (in >= 1 && in <= 9) {
        return in;
    } else if (in < 1) {
        return 1;
    }
    return 9;
}
