//
//  ConsoleUI.hpp
//  TicTacToe
//
//  Created by Bernhard Bauer on 05/11/2017.
//  Copyright © 2017 bbmk IT solutions gmbh. All rights reserved.
//

#ifndef ConsoleUI_hpp
#define ConsoleUI_hpp

#include <iostream>
#include <math.h>
#include "Player.hpp"

using namespace std;

class ConsoleUI {
    
    public:
        void drawFromArray(Player ***draw, int game_size);
    
};

#endif /* ConsoleUI_hpp */
